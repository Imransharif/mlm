@extends('layout.admin_layout')
@section('title','Network')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Payment Details</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-1">
         </div>
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Enrolment Details</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <div class="table-responsive">
                     <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                        <thead>
                           <tr class="info">
                              <th>First name</th>
                              <th>Surname</th> 
                              <th>Username</th>
                              <th>Mail</th>  
                              <th>Sponsor </th>                                       


                           </tr>
                        </thead>
                        <tbody>
                           @foreach(Session::get('step2') as $value)
                           <tr>
                              <td>{{$value['user_fname']}}</td>
                              <td>{{$value['user_sname']}}</td>
                              <td>{{$value['user_username']}}</td>
                              <td>{{$value['user_email']}}</td>
                              <td>{{$value['user_sopnser_id']}}</td>

                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-sm-1">
         </div>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-1">
         </div>
         <div class="col-sm-10">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Payment</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <form class="col-md-12" action="{{route('network.store')}}" method="POST" enctype="multipart/form-data">
                     @csrf
                     <div class="col-xs-12 col-sm-12 col-md-6 m-b-20">
                        <div class="col-xs-4 p-0">
                           <!-- required for floating -->
                           <!-- Nav tabs -->
                           <ul class="nav nav-tabs tabs-left">
                              <li class="active"><a href="#tab6" data-toggle="tab">Bitcoin</a></li>
                              <li><a href="#tab7" data-toggle="tab">Perfect money</a></li>
                           </ul>
                        </div>
                        <div class="col-xs-8 p-0">
                           <!-- Tab panels -->
                           <div class="tab-content">
                              <div class="tab-pane fade in  active" id="tab6">
                                 <div class="panel-body">
                                    <div class="form-group">
                                       <label>Card No*</label>
                                       <input type="text" class="form-control" name="bcoin" placeholder="Card No">
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-pane fade" id="tab7">
                                 <div class="panel-body">
                                  <div class="form-group">
                                    <label>Card No*</label>
                                    <input type="text" class="form-control" name="pmoney" placeholder="Card No*" >
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     

                     <div class="clearfix"></div>

                  </div>
                  
               </div>
            </div>
         </div>
         <div class="col-sm-1">
         </div>
      </div>
   </section>
   <section class="content">
      <div class="row">
       <div class="col-sm-1">
       </div>
       <div class="col-sm-10 col-md-10">
         <div class="panel panel-bd lobidisable">
            <div class="panel-heading">
               <div class="panel-title">
                  <h4>Package List</h4>
               </div>
            </div>
            <div class="panel-body">
               <div class="skin-square">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="i-check">

                           @if(isset($data))
                           @foreach($data as $value)
                           <div class="col-sm-4">
                           <input tabindex="11" type="radio" id="user_package{{$value->package_id}}" name="user_package" value="{{$value->package_id}}" onclick="package('{{$value->package_id}}')">
                           <label for="{{$value->package_id}}">{{$value->package_name}}
                              <span>&nbsp;&nbsp;</span>({{$value->package_amount}})</label>
                         </div>
                           @endforeach
                           @endif
                        </div>
                     </div>
                     <div class="col-md-12"> 
                        <div class="row">
                           <div class="col-md-4"></div>
                           <div class="col-md-6">
                              <br>
                                 <div class="reset-button">
                        <input type="submit" name="submit" value="Submit your Information" class="btn btn-success" >
                     </div>
                           </div>
                        </div>
                  
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-1">
   </div>
</div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@push('post-scripts')
<script type="text/javascript">
   function package(id) 
   {
      alert(id);
      var user_package=$('#user_package'+id).val();
      alert(user_package);
      if(user_package!='')
      {
         
      alert($('#user_package').val());
      $.ajax
      ({
         url:'{{route("network.store")}}',
         method:'POST',
         dataType:'json',
         data:{user_package:user_package},
         success:function(data) 
         {
            
        }
    });
     }
   }

</script>

@endpush

@extends('layout.admin_layout')
@section('title','Network')
@section('content')
         <!-- =============================================== -->
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
               <div class="header-icon">
                  <i class="fa fa-users"></i>
               </div>
               <div class="header-title">
                  <h1>Payment Details</h1>
               </div>
            </section>
                        <!-- Main content -->
            <section class="content">
               <div class="row">
                  <div class="col-sm-1">
                  </div>
                  <div class="col-sm-10">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Enrolment Details</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>First name</th>
                                       <th>Surname</th> 
                                       <th>Username</th>
                                       <th>Mail</th>  
                                       <th>Sponsor </th>                                       


                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach(Session::get('step2') as $value)
                                    <tr>
                                       <td>{{$value['user_fname']}}</td>
                                       <td>{{$value['user_sname']}}</td>
                                       <td>{{$value['user_username']}}</td>
                                       <td>{{$value['user_email']}}</td>
                                       <td>{{$value['user_sopnser_id']}}</td>

                                   </tr>
                                           @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-1">
                  </div>
               </div>
            </section>
            <!-- Main content -->
            <section class="content">
               <div class="row">
                  <!-- Form controls -->
                  <div class="col-sm-1">
                  </div>
                  <div class="col-sm-10">
                     <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                           <div class="btn-group" id="buttonexport">
                              <a href="#">
                                 <h4>Address of communication</h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body">
                           <form class="col-sm-6" action="{{route('network.get_value_using_session_step3')}}" method="POST" enctype="multipart/form-data">
                              @csrf
                              <div class="form-group">
                                 <label>Country *</label>
                                 <select class="form-control" name="country">
                                    <option value="pakistan">pakistan</option>
                                    <option value="amercia">amercia</option>
                                    <option value="sudia">sudia</option>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>Address 1 *</label>
                                 <input type="text" class="form-control" name="address1" placeholder="Enter Address 1" required>
                              </div>
                              <div class="form-group">
                                 <label>City*</label>
                                 <input type="text" class="form-control" name="city" placeholder="Enter City" required>
                              </div>

                              <div class="reset-button">
                                 <input type="submit" name="submit" value="Click for next step" class="btn btn-success">
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-1">
                  </div>
               </div>
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->
         @endsection
      
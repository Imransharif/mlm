@extends('layout.admin_layout')
@section('title','Network')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Add Member</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonlist"> 
                     <a class="btn btn-add " href="{{route('network.index')}}"> 
                        <i class="fa fa-list"></i>  Member List </a>  
                     </div>
                  </div>
                  <div class="panel-body">
                     <form class="col-sm-6" action="{{route('network.get_value_using_session2')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                           <label>Country *</label>
                           <select class="form-control" name="country">
                              @if(isset($data))
                              @foreach($data as $value)
                              <option value="{{$value->country_name}}">{{$value->country_name}}</option>
                              @endforeach
                              @endif
                              
                           </select>
                        </div>
                        <div class="form-group">
                           <label>First Name</label>
                           <input type="text" class="form-control" name="fname" placeholder="Enter First Name" required>
                        </div>
                        <div class="form-group">
                           <label>Last Name</label>
                           <input type="text" class="form-control" name="lname" placeholder="Enter First Name" required>
                        </div>

                        <div class="form-group">
                           <label>Surname *</label>
                           <input type="text" class="form-control" name="sname" placeholder="Enter Surname" required>
                        </div>
                        <div class="form-group">
                           <label>Username *</label>
                           <input type="text" class="form-control" name="user_username" placeholder="Enter Username" required>
                        </div>
                        @if ($errors->has('user_username'))
                        <div style="color: red">{{ $errors->first('user_username') }}</div>
                        @endif
                        <div class="form-group">
                           <label>Email</label>
                           <input type="email" class="form-control" name="email" placeholder="Enter Email" required>
                        </div>
                        <div class="form-group">
                           <label>Password</label>
                           <input type="Password" class="form-control" name="password" placeholder="Enter Password" required>
                        </div>
                        <div class="form-group">
                           <label>Confirm Password</label>
                           <input type="Password" class="form-control" name="ConfirmPassword" placeholder="Confirm Password" required>
                        </div>
                        @if ($errors->has('password'))
                        <div style="color: red">{{ $errors->first('password') }}</div>
                        @endif
                        <div class="form-group">
                           <label>Sponser</label>
                           <input type="text" class="form-control" name="sponser" placeholder="Enter Bank details" value="{{Auth::user()->user_username}}" readonly="">
                        </div>
                        <div class="form-group">
                           <label>Mobile</label>
                           <input type="text" class="form-control" name="phone" placeholder="Enter Mobile" required>
                        </div>
                        <div class="form-group">
                           <label>Picture upload</label>
                           <input type="file" name="picture">
                           <input type="hidden" name="old_picture">
                        </div>

                        <div class="reset-button">
                           <input type="submit" name="submit" value="Click for next step" class="btn btn-success">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
   @endsection
   
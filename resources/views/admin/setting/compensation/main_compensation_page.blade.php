@extends('layout.admin_layout')
@section('title','Compensation')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-money"></i>
      </div>
      <div class="header-title">
         <h1>System Configurations </h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Configurations</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">

                  <div class="table-responsive">
                     <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                        <thead>                     
                           <tr class="info">
                              <th>Type of Compensations</th>
                              <th>Status</th>
                              <th>Configuration</th>
                           </tr>
                        </thead>
                        <tbody>
                          @if(isset($data))
                          @foreach($data as $value)
                           <tr>
                           <td>{{$value->compensation_name}}</td>
                              <td>
                                 <div class="toggle-example">
                                   <input type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-size="small">
                                </div>
                             </td>
                             <td>
                              <button type="button" class="btn btn-primary btn-circle m-b-3"><a href="{{url($value->compensation_url)}}/{{$value->compensation_id}}" class="glyphicon glyphicon-cog" style="color: #fff;"> </a></button></td>
                           </tr>
                            @endforeach
                          @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@extends('layout.admin_layout')
@section('title','Packages')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Package</h1>
         <small>Package List</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>All Package</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                  <div class="btn-group">
                     <div class="buttonexport" id="buttonlist"> 
                        <a class="btn btn-add" href="{{route('package.create')}}"> <i class="fa fa-plus"></i> Add Package
                        </a>  
                     </div>
                     <button class="btn btn-exp btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> Export Table Data</button>
                     <ul class="dropdown-menu exp-drop" role="menu">
                        <li class="divider"></li>
                        <li>
                           <a href="#" onclick="$('#dataTableExample1').tableExport({type:'doc',escape:'false'});">
                              <img src="assets/dist/img/word.png" width="24" alt="logo"> Word</a>
                           </li>
                           <li>
                              <a href="#" onclick="$('#dataTableExample1').tableExport({type:'powerpoint',escape:'false'});"> 
                                 <img src="assets/dist/img/ppt.png" width="24" alt="logo"> PowerPoint</a>
                              </li>
                              <li class="divider"></li>
                              <li>
                                 <a href="#" onclick="$('#dataTableExample1').tableExport({type:'pdf',pdfFontSize:'7',escape:'false'});"> 
                                    <img src="assets/dist/img/pdf.png" width="24" alt="logo"> PDF</a>
                                 </li>
                              </ul>
                           </div>
                           <!-- Plugin content:powerpoint,txt,pdf,png,word,xl -->
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                    <tr class="info">
                                       <th>Package Name</th>
                                       <th>Package Amount</th>
                                       <th>Package Period</th>
                                       <th>Package Tax</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if(isset($data))
                                    @foreach($data as $value)
                                    <tr>
                                       <td>{{$value->package_name}}</td>
                                       <td>{{$value->package_amount}}</td>
                                       <td>{{$value->package_period}}</td>
                                       <td>{{$value->package_tax}}</td>
                                       @if($value->status==1)
                                       <td><a href="javascript:;" onclick="change_status(this,'{{$value->package_id}}',1)" class="btn btn-success btn-xs"> Active </a></td>
                                       
                                       @else
                                       <td><a href="javascript:;" onclick="change_status(this,'{{$value->package_id}}',0)" class="btn btn-danger btn-xs"> Deactive </a></td>

                                       @endif
                                       <td><a href="{{url('edit_package/'.$value->package_id)}}" class="btn btn-primary btn-sm" title="Click for update package"> <i class="glyphicon glyphicon-edit"></i></a></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

            </section>
            <!-- /.content -->
         </div>
         @endsection
         @push('post-scripts')
         <script type="text/javascript">
           function change_status(obj,id,status)
           {
             $.ajax({
               url: "{{route('package.status_update')}}",
               method:"POST",
               data:{'id':id,'status':status},
               success: function(response){
                 if(response.status){
                   if(status){
                     $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',0)" class="btn btn-danger btn-xs"> Deactive </a>');
                     toastr.error(response.message);
                  }else{
                     $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',1)" class="btn btn-success btn-xs"> Active </a>');
                     toastr.success(response.message);
                  }
               }else{
                toastr.error(response.message);
             }
          }
       });
          }
       </script>
       @endpush
@extends('layout.admin_layout')
@section('title','Admin bonus Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-gears"></i>
      </div>
      <div class="header-title">
         <h1>Admin bonus Settings</h1>
         <small>Admin bonus Settings</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Admin bonus Settings</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <form class="col-sm-6" action="{{route('admin.update')}}" method="POST" enctype="multipart/form-data">
                     @csrf
                     @foreach($data as $value)
                     <div class="form-group">
                        <label>Admin Bonus</label>
                        <input type="number" class="form-control" placeholder="Enter Name" name="admin__bonus" value="{{$value->admin__bonus}}">

                        <input type="hidden" name="id" value="{{$value->admin_bonus_id}}">
                     </div>
                     <div class="form-group">
                        <label>User Name</label>
                        <input type="text" id="user_name" class="form-control" placeholder="Enter Name" name="user_name" value="{{$value->user_name}}" onkeyup="verify_name()">
                        <span style="color:red" id="append_data"></span>
                     </div>
                     <div class="form-check">
                        <label>Status</label><br>
                        <label class="radio-inline">
                           <input type="radio" name="status" value="1" checked="checked">Active</label>
                           <label class="radio-inline"><input type="radio" name="status" value="0" >Inctive</label>
                        </div>
                        <div class="reset-button">
                           <input type="submit" name="submit" class="btn btn-add">
                        </div>
                        @endforeach
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- /.content -->
   </div>
   @endsection

   @push('post-scripts')
   <script type="text/javascript">
      function verify_name() 
      {
         var user_name=$('#user_name').val();
         $.ajax
         ({
            url:'{{route("plan.verify_user")}}',
            method:'POST',
            dataType:'json',
            data:{user_name:user_name},
            success:function(data) 
            {
               $('#append_data').empty();
               $("#append_data").append("<b>"+data.message+"</b>");
            }
         })
      }
   </script>

   @endpush

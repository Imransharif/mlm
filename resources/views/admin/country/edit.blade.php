@extends('layout.admin_layout')
@section('title','Country Setting')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-gears"></i>
      </div>
      <div class="header-title">
         <h1>Country Settings</h1>
         <small>Country Settings</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Country Settings</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                  <form class="col-sm-6" action="{{route('country.update')}}" method="POST" enctype="multipart/form-data">
                     @csrf
                     <input type="hidden" name="id" value="{{$data->country_id}}">
                     <input type="hidden" name="old_image" value="{{$data->country_image}}">
                     <div class="form-group">
                        <label>Country Name</label>
                        <input type="text" class="form-control" placeholder="Enter Name" name="country_name" value="{{$data->country_name}}">
                        
                     </div>
                     <div class="form-group">
                        <label>Country Code</label>
                        <input type="number" class="form-control" placeholder="Enter Name" name="country_code" value="{{$data->country_code}}">
                     </div>
                     <td><img src="images/{{$data->country_image}}" class="img-circle" alt="User Image" width="60" height="60"> </td>
                     <div class="form-group">
                        <label>Change Site Logo</label>
                        <input type="file" name="picture">
                        <input type="hidden" name="old_picture">
                     </div>
                     <div class="reset-button">
                        <input type="submit" name="submit" class="btn btn-add">
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
@endsection

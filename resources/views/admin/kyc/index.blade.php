@extends('layout.admin_layout')
@section('title','Kyc Document')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Kyc Document</h1>
         <small>Kyc Document List</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-bd lobidrag">
               <div class="panel-heading">
                  <div class="btn-group" id="buttonexport">
                     <a href="#">
                        <h4>Kyc Document</h4>
                     </a>
                  </div>
               </div>
               <div class="panel-body">
                           <div class="table-responsive">
                              <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                 <thead>
                                  <tr class="info">
                                       <th>User Name</th>
                                       <th>Photo</th>
                                       <th>Status</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if(isset($data))
                                    @foreach($data as $value)
                                    <tr>
                                    <td>{{$value->user_name}}</td>
                                    <td><img src="images/{{$value->kyc_document}}" class="img-circle" alt="User Image" width="70" height="70"> 
                                    </td>
                                       @if($value->status==1)
                                       <td><a href="javascript:;" onclick="change_status(this,'{{$value->kyc_id}}',1)" class="btn btn-success btn-ms"> Verified </a></td>
                                       @else
                                       <td><a href="javascript:;" onclick="change_status(this,'{{$value->kyc_id}}',0)" class="btn btn-danger btn-ms"> Unverified </a></td>
                                       @endif
                                       <td><button type="button" class="btn btn-purple  btn-circle m-b-5"><a href="/download_image/{{$value->kyc_document}}"><i style="color: white" class="glyphicon glyphicon-download-alt"></i></a></button></td>

                                    </tr>
                                    @endforeach
                                    @endif
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.modal -->
            </section>
            <!-- /.content -->
         </div>
         @endsection
                  @push('post-scripts')

         <script type="text/javascript">
            $(document).ready(function() {
             $('#dataTableExample1').DataTable();
             });
            function change_status(obj,id,status)
            {
             $.ajax({
               url: "{{route('kyc.status_update')}}",
               method:"POST",
               data:{'id':id,'status':status},
               success: function(response){
                 if(response.status){
                   if(status){
                     $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',0)" class="btn btn-danger btn-ms"> Unverified </a>');
                     toastr.error(response.message);
                  }else{
                     $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',1)" class="btn btn-success btn-ms"> Verified </a>');
                     toastr.success(response.message);
                  }
               }else{
                toastr.error(response.message);
             }
          }
       });
          }
       </script>
       @endpush

   @extends('layout.admin_layout')
@section('title','Network')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon">
         <i class="fa fa-users"></i>
      </div>
      <div class="header-title">
         <h1>Buy Plan</h1>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <!-- Form controls -->
      <div class="col-sm-12
         ">
            <div class="panel panel-bd lobidrag">
               <div class="panel-body">
                  <form class="col-sm-6" action="{{route('network.buyplane')}}" method="POST" enctype="multipart/form-data">
                     @csrf

                     @if(Session::has('message'))
                      <div class="form-group">
                        <div class="alert alert-sm alert-danger alert-dismissible" role="alert">
                           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <i class="fa fa-exclamation-circle"></i><strong>Danger!</strong>{{ Session::get('message') }}
                        </div>
                     </div>
                     @endif
                     <div class="form-group">
                        <label>Country *</label>
                        <select class="form-control" name="country">
                           <option value="pakistan">pakistan</option>
                           <option value="amercia">amercia</option>
                           <option value="sudia">sudia</option>
                        </select>
                     </div>

                     <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" name="fname" placeholder="Enter First Name" required>
                     </div>
                     <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" name="lname" placeholder="Enter First Name" required>
                     </div>

                     <div class="form-group">
                        <label>Surname *</label>
                        <input type="text" class="form-control" name="sname" placeholder="Enter Surname" required>
                     </div>
                     <div class="form-group">
                        <label>Username *</label>
                        <input type="text" class="form-control" name="user_username" placeholder="Enter Username" required>
                     </div>
                     @if ($errors->has('user_username'))
                     <div style="color: red">{{ $errors->first('user_username') }}</div>
                     @endif
                     <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Enter Email" required>
                     </div>
                     <div class="form-group">
                        <label>Password</label>
                        <input type="Password" class="form-control" name="password" placeholder="Enter Password" required>
                     </div>
                     <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="Password" class="form-control" name="ConfirmPassword" placeholder="Confirm Password" required>
                     </div>
                     @if ($errors->has('password'))
                     <div style="color: red">{{ $errors->first('password') }}</div>
                     @endif
                     <div class="form-group">
                        <label>Referal Code</label>
                        <input type="text" class="form-control" name="rcode" placeholder="Enter Referal Code" required="">
                     </div>
                     <div class="form-group">
                        <label>Mobile</label>
                        <input type="text" class="form-control" name="phone" placeholder="Enter Mobile" required>
                     </div>
                     <div class="form-group">
                        <label>Picture upload</label>
                        <input type="file" name="picture">
                        <input type="hidden" name="old_picture">
                     </div>
                     <div class="form-check">
                        <label>Select Package</label><br>
                        <label class="radio-inline">
                           <input type="radio" name="package" value="1" checked="checked">Starter(0$)</label>
                           <label class="radio-inline">
                              <input type="radio" name="package" value="2" checked="checked">Basic($25)</label>
                              <label class="radio-inline">
                                 <input type="radio" name="package" value="3" checked="checked">Advance($40)</label>
                                 <label class="radio-inline">
                                    <input type="radio" name="package" value="4" checked="checked">Elite($50)</label>
                                    <label class="radio-inline">
                                       <input type="radio" name="package" value="5" checked="checked">Ultra($80)</label>
                                       <label class="radio-inline">
                                          <input type="radio" name="package" value="6" checked="checked">Master($100)</label>
                                          <label class="radio-inline">
                                             <input type="radio" name="package" value="7" checked="checked">Premium($150)</label>
                                             <label class="radio-inline">
                                                <input type="radio" name="package" value="8" checked="checked">Gold($250)</label>
                                                <label class="radio-inline">
                                                   <input type="radio" name="package" value="9" checked="checked">Plantiniuum($250)</label>                    
                                                </div>
                                                <div class="reset-button">
                                                   <input type="submit" name="submit" value="Click for next step" class="btn btn-success">
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                              <!-- /.content -->
                           </div>
                           <!-- /.content-wrapper -->
                           @endsection
                           @push('post-scripts')
                           <script type="text/javascript">
                             function change_status(obj,id,status)
                             {
                                alert();
                             }
                          </script>
                          @endpush
@extends('layout.admin_layout')
@section('title','Prifile ')
@section('content')
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="header-icon"><i class="fa fa-user-circle-o"></i></div>
      <div class="header-title">
         <h1>Profile</h1>
         <small>Show user data in clear profile design</small>
      </div>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-sm-12 col-md-4">
            <div class="card">
               <div class="card-header">
                  <img src="images/{{Auth::user()->user_image}}" class="img-circle" width="100" height="100" alt="user">
               </div>
               <div class="card-content">
                  <div class="card-content-member text-center">
                     <h4 class="m-t-0">{{Auth::user()->user_username}}</h4>
                     <p class="m-t-0">{{Auth::user()->email}}</p>
                  </div>
                  <div class="card-content-languages">
                     <div class="card-content-languages-group">
                        <div>
                           <h4>Package:</h4>
                        </div>
                        <div>
                           <ul>
                              <li>  
                                 <tr>
                                    @if(Auth::user()->user_package==1)
                                    <td>Starter</td>
                                    <td>$0</td>
                                    @endif
                                    @if(Auth::user()->user_package==2)
                                    <td>Basic</td>
                                    <td>$25</td>
                                    @endif
                                    @if(Auth::user()->user_package==3)
                                    <td>Advance</td>
                                    <td>$40</td>
                                    @endif
                                    @if(Auth::user()->user_package==4)
                                    <td>Elite</td>
                                    <td>$50</td>
                                    @endif
                                    @if(Auth::user()->user_package==5)
                                    <td>Ultra</td>
                                    <td>1</td>
                                    <td>$80</td>
                                    @endif
                                    @if(Auth::user()->user_package==6)
                                    <td>Master</td>
                                    <td>$100</td>
                                    @endif
                                    @if(Auth::user()->user_package==7)
                                    <td>Premium</td>
                                    <td>$150</td>
                                    @endif
                                    @if(Auth::user()->user_package==8)
                                    <td>Gold</td>
                                    <td>$250</td>
                                    @endif
                                    @if(Auth::user()->user_package==9)
                                    <td>Plantiniuum</td>
                                    <td>$250</td>
                                    @endif
                                 </tr>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="card-content-languages-group">
                        <div>
                           <h4>Phone:</h4>
                        </div>
                        <div>
                           <ul>
                              <li>{{Auth::user()->user_phone}}</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="card-content-summary">
                     <p>{{Auth::user()->user_address}}.</p>
                  </div>
               </div>
               <div class="card-footer">
                  <div class="card-footer-stats">
                     <div>
                        <p>Total Member:</p>
                        <i class="fa fa-users"></i><span>241</span>
                     </div>
                     <div>
                        <p>MESSAGES:</p>
                        <i class="fa fa-coffee"></i><span>350</span>
                     </div>
                     <div>
                        <p>Last online</p>
                        <span class="stats-small">3 days ago</span>
                     </div>
                  </div>
                  <div class="card-footer-message">
                     <h4></h4>
                  </div>
               </div>
            </div>
         </div>
         <section class="content">
            <div class="row">
               <!-- Form controls -->
               <div class="col-sm-1">
               </div>
               <div class="col-sm-6">
                  <div class="panel panel-bd lobidrag">
                     <div class="panel-heading">
                        <div class="btn-group" id="buttonexport">
                           <a href="#">
                              <h4>Edit Profile</h4>
                           </a>
                        </div>
                     </div>
                     <div class="panel-body">
                        <form class="col-sm-6" action="{{route('network.update')}}" method="POST" enctype="multipart/form-data">
                           @csrf
                           <input type="hidden" name="id" value="{{Auth::user()->id}}">
                           <div class="form-group">
                              <label>User Name </label>
                              <input type="text" class="form-control" name="user_username" placeholder="Enter Address 1" value="{{Auth::user()->user_username}}">
                           </div>
                            @if ($errors->any())
                           <div style="color: red">
                            <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                           </ul>
                        </div>
                        @endif
                           <div class="form-group">
                              <label>Email </label>
                              <input type="text" class="form-control" name="email" placeholder="Enter Address 1" value="{{Auth::user()->email}}">
                           </div>
                           <div class="form-group">
                              <label>Phone*</label>
                              <input type="text" class="form-control" name="phone" placeholder="Enter City" value="{{Auth::user()->user_phone}}">
                           </div>
                           <input type="hidden" name="picture" value="{{Auth::user()->user_image}}">
                           <div class="form-group">
                              <label>Picture upload</label>
                              <input type="file" name="picture">
                              <input type="hidden" name="old_picture">
                           </div>

                        <div class="reset-button">
                           <input type="submit" name="submit" value="Update Profile" class="btn btn-success">
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <div class="col-sm-1">
            </div>
         </div>
      </section>
   </div>
</section>
<!-- /.content -->
</div>
@endsection

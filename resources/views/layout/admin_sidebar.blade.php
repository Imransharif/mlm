<aside class="main-sidebar">
   <!-- sidebar -->
   <div class="sidebar">
      <!-- sidebar menu -->
      <ul class="sidebar-menu">
         <li class="active">
            <a href="{{route('home')}}"><i class="fa fa-tachometer"></i><span>{{__('dashboard.dashboard_name')}}</span>
               <span class="pull-right-container">
               </span>
            </a>
         </li>
         @if (Auth::user()->role=='admin')
         <li class="treeview">
            <a href="#">
               <i class="fa fa-users"></i><span>{{__('dashboard.network')}}</span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="{{route('network.create')}}">Add New Member </a></li>
               <li><a href="{{route('genealogy-tree.index')}}">Genealogy Tree </a></li> 
               <li><a href="group.html">Network Explorer </a></li>
               <li><a href="group.html">Downline Members</a></li>

            </ul>
         </li>
          <li class="treeview">
            <a href="#">
               <i class="fa fa-shopping-cart"></i><span>Country</span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="{{route('country.index')}}">List</a></li>
               <li><a href="{{route('country.inactive_country')}}">Unactive</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-users"></i><span>Member Management </span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="{{route('network.active_member')}}">Active Members </a></li>
               <li><a href="{{route('network.suspended_member')}}">Suspended Members </a></li>
               <li><a href="transfer.html">Unpaid Members </a></li>
               <li><a href="{{route('network.deactive_member')}}">Unverified Members </a></li>
               <li><a href="{{route('kyc_document.user_kyc')}}">KYC </a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-shopping-cart"></i><span>E-Wallet</span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="invoice.html">Account Summary </a></li>
               <li><a href="ninvoices.html">Deposit Request </a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-shopping-bag"></i><span>Payout </span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="cpayment.html">Pending Payment</a></li>
               <li><a href="emanage.html">Completed Payment </a></li>
               <li><a href="ecategory.html">Release Payout </a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-key"></i><span>T-Pin</span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="jpost.html">Tracking T-Pins </a></li>
               <li><a href="japp.html">T-Pin Configuration </a></li>
               <li><a href="japp.html">Generate T-Pin</a></li>
               <li><a href="japp.html">Available T-Pin History</a></li>
               <li><a href="japp.html">Assign T-Pin</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-gear"></i>
               <span>{{__('dashboard.setting')}}</span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="{{route('setting.index')}}">Site Settings</a></li>
               <li><a href="{{route('network.sub_admin_list')}}">Sub Admin</a></li>
               <li><a href="emailsetting.html">Captcha Setting</a></li>
               <li><a href="paysetting.html">Wallet Configuration</a></li>
               <li><a href="{{route('network.email_setting')}}">Email Setting </a></li>
               <li><a href="{{route('package.manage_package')}}">Manage Packages </a></li>
               <li><a href="{{route('package.index')}}">Investment Packages </a></li>
               <li><a href="{{route('admin.index')}}">Admin Bonus </a></li> 
               <li><a href="{{route('admin_penalty.index')}}">Admin Penalty </a></li>
               <li><a href="{{url('language_setting')}}">Language Setting </a></li>  
               <li><a href="{{route('network.currency_setting')}}">Currency Setting</a></li> 
               <li><a href="{{route('compensation.show')}}">Compensation</a></li>
               <li><a href="{{url('database-backup')}}">DB Backup/Restore </a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-file-text"></i><span>Report</span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="preport.html">Transaction History</a></li>
               <li><a href="creport.html">Commission History</a></li>
               <li><a href="ereport.html">Paid History</a></li>
               <li><a href="incomexp.html">Rank Analysis </a></li>
               <li><a href="incomexp.html">Deposit History </a></li>
               <li><a href="incomexp.html">Interest History </a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-bell"></i><span>Renewal </span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="thistory.html">Renewal Users </a></li>
               <li><a href="timechange.html">4x Qualify Users </a></li>

            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-edit"></i><span>Payment Support </span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="jpost.html">Bitcoin</a></li>
               <li><a href="japp.html">Perfect Money</a></li>
            </ul>
         </li>
          @endif
          @if (Auth::user()->role=='admin' || Auth::user()->role=='user')
         <li class="treeview">
            <a href="#">
               <i class="fa fa-user"></i><span>Profile</span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="{{route('network.show_profile')}}">My Profile </a></li>
               <li><a href="hourly.html">Google Authenticator </a></li>
               <li><a href="managesal.html">E-mail Setting </a></li>
               <li><a href="empsallist.html">My Wallet </a></li>
               <li><a href="{{route('kyc_document.index')}}">KYC Document</a></li>
               <li><a href="{{route('network.changepassword')}}">Change Password </a></li>
               <li><a href="generatepay.html">Change T-Pin</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-money"></i><span>Financial</span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="stockcat.html">Financial Extract </a></li>
               <li><a href="manstock.html">Pay HASH </a></li> 
               <li><a href="astock.html">Request Withdraw </a></li>
               <li><a href="{{route('transfer-history.history')}}">Member to Member transfer</a></li>
               <li><a href="astock.html">Hash History</a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-users"></i><span>My Network </span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="ticanswer.html">Matrix Network/My Genealogy </a></li>
               <li><a href="ticopen.html">Qualification Plan </a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-list"></i><span>Reward Bonus </span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
            <li><a href="{{route('referal.direct_referal_bonus')}}">Direct Referral Bonus </a></li>
               <li><a href="{{route('referal.matrix_bonus')}}">Matrix Bonus</a></li>
               <li><a href="{{route('residul.residul_bonus')}}">Residual Bonus</a></li>
               <li><a href="systemsts.html">Renewal 4x Bonus </a></li>
            </ul>
         </li>
         <li class="treeview">
            <a href="#">
               <i class="fa fa-bar-chart"></i><span>Investment Plans</span>
               <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
               </span>
            </a>
            <ul class="treeview-menu">
               <li class=""><a href="{{route('network.buy_plan')}}">Buy Plan </a></li>
               <li><a href="charts_Js.html">Upgrade Plan </a></li>
            </ul>
         </li>
          @endif
      </ul>
   </div>
   <!-- /.sidebar -->
</aside>
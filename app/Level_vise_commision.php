<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level_vise_commision extends Model
{
    protected $fillable = array('cp_id', 'cp_level_percentage');
}

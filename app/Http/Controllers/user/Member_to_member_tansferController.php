<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Member_to_member_transfer;
use App\Fund_transfer_setting;
use Session;

class Member_to_member_tansferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Member_to_member_transfer::where('status',1)->get();
        return view('user.finanical.member_to_member_trasfer.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('user.finanical.member_to_member_trasfer.add');
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = array
        (
            'mtmt_amount' =>$request->transfer_amount,
            'transferto_user_name' =>$request->user_name,
            'transferby_user_name' =>Auth::user()->user_username
        );
        $insert=Member_to_member_transfer::insert($data);
        if($insert)
        {
            return redirect()->route('transfer-history.history');
        }


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Member_to_member_transfer::where('mtmt_id',$id)->first();
        return view('user.finanical.member_to_member_trasfer.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->id;
        $data = array
        (
            'mtmt_amount' =>$request->transfer_amount,
            'transferto_user_name' =>$request->user_name,
            'transferby_user_name' =>Auth::user()->user_username
        );
        $update_data=Member_to_member_transfer::where('mtmt_id',$id)->update($data);
        if($update_data)
        {
            return redirect()->route('transfer-history.history');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify_trafer_amount(Request $request)
    {
        $get=Fund_transfer_setting::where('status',1)->first();
        if($request->user_amount>$get->minimum_transfer_amount && 
            $request->user_amount<$get->maximum_transfer_amount )
        {
           $response['status']= 1;
           $response['message']="These credentials match our records.";
       }
       else
       {
          $response['status']= 0;
          $response['message']="These credentials do not match our records.";  
      }

      return response()->json($response, 200);
  }


  public function status_update(Request $request)
  {

     $id = $request->input('id');
     $status = $request->input('status');
     if($status==1)
     {
      $input['status']=0;
  }
  else

  {
     $input['status']=1;
 }
 $effected=Member_to_member_transfer::where('mtmt_id',$id)->update($input);
 $response = array();
 if($status == 1)
 {
     $response['status']= true;
     $response['message']="Transfer amount Cancenled ";
 }else
 {
     $response['status']= true;
     $response['message']="Request of Transfer amount active ";
 }
 return response()->json($response, 200);

}
}

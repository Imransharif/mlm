<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Kyc;
use Auth;
use Response;

class KycController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = kyc::where([
           'user_name' =>Auth::user()->user_username,
       ])->get();
        return view('user.kyc.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.kyc.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       if(Input::hasFile('picture'))
       {
         $image=Input::file('picture');
         $name=time().'.'.$image->getClientOriginalExtension();
         $destinationpath=public_path('images/');
         $image->move($destinationpath,$name);
         $imagess=$name;   
     }
     $insert=Kyc::create([
        'kyc_document' =>$imagess,
        'user_name' => Auth::user()->user_username,
        'slug'=>str_slug(Auth::user()->user_username, '-'),
    ]);
     if($insert)
     {
        return redirect()->route('kyc_document.index');
    }

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {

        $data = kyc::where([
           'slug' =>$slug,
       ])->first();
        return view('user.kyc.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $id=$request->id;
        if(Input::hasFile('picture'))
        {
         $image=Input::file('picture');
         $name=time().'.'.$image->getClientOriginalExtension();
         $destinationpath=public_path('images/');
         $image->move($destinationpath,$name);
         $imagess=$name;   
     }
     $updated_data=Kyc::where('kyc_id',$id)->update(
        [
            'kyc_document'=>$imagess
        ]
    );
     if($updated_data)
     {
        return redirect()->route('kyc_document.index');
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function user_kyc()
    {
        $data = kyc::get();
        return view('admin.kyc.index',compact('data'));
    }
    public function download_image($image)
    {
        $filepath = public_path('images/')."$image";
        return Response::download($filepath);
    }

    public function status_update(Request $request)
    {

       $id = $request->input('id');
       $status = $request->input('status');
       if($status==1)
       {
          $input['status']=0;
      }
      else

      {
       $input['status']=1;
   }
   $effected=Kyc::where('kyc_id',$id)->update($input);
   $response = array();
   if($status == 1)
   {
       $response['status']= true;
       $response['message']="Verified";
   }else
   {
       $response['status']= true;
       $response['message']="Unverified";
   }
   return response()->json($response, 200);

}
}

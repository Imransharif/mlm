<?php

namespace App\Http\Controllers\admin\package;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=orderBy('package_id', 'DESC')->get();
        return view('admin.setting.package.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.setting.package.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array
        (
            'package_name' =>$request->package_name,
            'package_amount' =>$request->package_amount,
            'package_period' =>$request->package_period,
            'package_tax' =>$request->package_tax,
            'status' =>$request->status  
        );
        $insert=Package::insert($data);
        if($insert)
        {
            return redirect()->route('package.manage_package');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Package::where('package_id',$id)->get();
        return view('admin.setting.package.edit',compact('data'));
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->id;
        $data = array
        (
            'package_name' =>$request->package_name,
            'package_amount' =>$request->package_amount,
            'package_period' =>$request->package_period,
            'package_tax' =>$request->package_tax,
            'status' =>$request->status  
        );
        $update_data=Package::where('package_id',$id)->update($data);
        if($update_data)
        {
            return redirect()->route('package.manage_package');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function status_update(Request $request)
    {

       $id = $request->input('id');
       $status = $request->input('status');
       if($status==1)
       {
          $input['status']=0;
      }
      else

      {
       $input['status']=1;
   }
   $effected=Package::where('package_id',$id)->update($input);
   $response = array();
   if($status == 1){
       $response['status']= true;
       $response['message']="Package has been Deactive .";
   }else{
       $response['status']= true;
       $response['message']="Package has been Active.";
   }
   return response()->json($response, 200);

   }
    public function manage_package()
    {
        $data=Package::orderBy('package_id', 'DESC')->get();
        return view('admin.setting.package.manage_package',compact('data'));
    }
}

<?php

namespace App\Http\Controllers\admin\residual;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Residual_bonus;
class ResidualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Residual_bonus::get();
        return view('admin.setting.residual.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.setting.residual.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array
        (
            'residual_bonus' =>$request->residual_bonus,
            'status'=>$request->status
        );
        $insert=Residual_bonus::insert($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data=Residual_bonus::where('residual_id',$id)->get();
        return view('admin.setting.residual.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
  
        $id=$request->id;
        $data = array
        (
            'residual_bonus' =>$request->residual_bonus,
            'status'=>$request->status 
        );
        $update_data=Residual_bonus::where('residual_id',$id)->update($data);
        if($update_data)
        {
            return redirect()->route('residual.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function residul_bonus()
    {
        $data=Residual_bonus::first();
        return view('user.residul_bonus',compact('data'));
    }
}

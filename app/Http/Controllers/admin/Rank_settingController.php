<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rank_Setting;

class Rank_settingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Rank_Setting::get();
        return view('admin.setting.ranksetting.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Rank_Setting::truncate();
        for ($i=0; $i <count($request->rank_title) ; $i++) 
        { 
            $data = 
            array
            (
             'rank_name'=>$request->rank_title[$i],
             'rank_amount'=>$request->rank_amount[$i]
         );
            $insert= Rank_Setting::insert($data);
        }

        return redirect()->route('rank.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Rank_Setting::where('rank_id',$id)->get();
        return view('admin.setting.ranksetting.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $id=$request->id;
       $data = array
       (
        'rank_name'=>$request->rank_name,
        'rank_amount'=>$request->rank_amount
    );
       $update_data=Rank_Setting::where('rank_id',$id)->update($data);
       if($update_data)
       {
        return redirect()->route('rank.index');
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;
use Illuminate\Support\Facades\Input;
class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data=Country::get();
      return view('admin.country.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.country.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     if(Input::hasFile('picture'))
     {
       $image=Input::file('picture');
       $name=time().'.'.$image->getClientOriginalExtension();
       $destinationpath=public_path('images/');
       $image->move($destinationpath,$name);
       $imagess=$name;   
     }
     else
     {
      $imagess='avatar5.jpg';
    }
    $data = 
    array(

      'country_name' =>$request->country_name,
      'country_code' =>$request->country_code,
      'country_image' =>$imagess,
      'country_status' =>$request->status,
    );
    $insert=Country::create($data);
    if($insert)
    {
      return redirect()->route('country.index');
    }
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data=Country::where('country_id',$id)->first();
      return view('admin.country.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
     if(Input::hasFile('picture'))
     {
       $image=Input::file('picture');
       $name=time().'.'.$image->getClientOriginalExtension();
       $destinationpath=public_path('images/');
       $image->move($destinationpath,$name);
       $imagess=$name;   
     }
     else
     {
      $imagess=$request->picture;
    }
    $id=$request->id;
    $data = 
    array(

      'country_name' =>$request->country_name,
      'country_code' =>$request->country_code,
      'country_image' =>$imagess,
    );
    $update_data=Country::where('country_id',$id)->update($data);
    if($update_data)
    {
      return redirect()->route('country.index');
    }
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function status_update(Request $request)
    {

     $id = $request->input('id');
     $status = $request->input('status');
     if($status==1)
     {
      $input['country_status']=0;
    }
    else

    {
     $input['country_status']=1;
   }
   $effected=Country::where('country_id',$id)->update($input);
   $response = array();
   if($status == 1)
   {
     $response['status']= true;
     $response['message']="Country has been Deactive .";
   }else
   {
     $response['status']= true;
     $response['message']="Country has been Active.";
   }
   return response()->json($response, 200);

 }

 public function inactive_country()
 {
  $data=Country::where('country_status',0)->get();
  return view('admin.country.index',compact('data'));
}
}

<?php

namespace App\Http\Controllers\admin\referal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Referal_bonus;

class ReferalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.setting.referal.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = array
        (
            'referal_bonus' =>$request->referal_amount,
            'status'=>$request->status
        );
        $insert=Referal_bonus::insert($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Referal_bonus::get();
        return view('admin.setting.referal.index',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data=Referal_bonus::where('referal_id',$id)->get();
        return view('admin.setting.referal.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id=$request->id;
        $data = array
        (
            'referal_bonus' =>$request->referal_bonus 
        );
        $update_data=Referal_bonus::where('referal_id',$id)->update($data);
        if($update_data)
        {
            return redirect()->route('referal.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function direct_referal_bonus()
    {
        $data=Referal_bonus::first();
        return view('user.direct_referal',compact('data'));
    }
}

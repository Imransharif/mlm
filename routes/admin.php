<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/view_network', 'admin\network\NetworkController@index')->name('network.index')->middleware('role');
Route::group(['prefix' => 'admin'], function () {
   Route::get('/add_network', 'admin\network\NetworkController@create')->name('network.create');
});
Route::group(['prefix' => 'admin'], function () {
   Route::get('/admin-login',function()
{
	return view('auth.admin_login');
});
});
Route::post('/store_network', 'admin\network\NetworkController@store')->name('network.store');
Route::post('/setp_2', 'admin\network\NetworkController@get_value_using_session_step2')->name('network.get_value_using_session2');
Route::post('/setp_3', 'admin\network\NetworkController@get_value_using_session_step3')->name('network.get_value_using_session_step3');
Route::get('/show_profile', 'admin\network\NetworkController@show_profile')->name('network.show_profile');
Route::post('/update_profile', 'admin\network\NetworkController@update')->name('network.update');
Route::post('/change_password', 'admin\network\NetworkController@change_password')->name('network.change_password');
Route::get('/password', 'admin\network\NetworkController@changepassword')->name('network.changepassword');
Route::get('/active_member', 'admin\network\NetworkController@active_member')->name('network.active_member');
Route::get('/Unverified_Members', 'admin\network\NetworkController@deactive_member')->name('network.deactive_member');
Route::post('/status_update', 'admin\network\NetworkController@status_update')->name('network.status_update');
Route::get('/buy_plan', 'admin\network\NetworkController@buy_plan')->name('network.buy_plan');
Route::post('/buyplane', 'admin\network\NetworkController@buyplane')->name('network.buyplane');
Route::get('/suspended_membr/{id}', 'admin\network\NetworkController@suspended_membr');
Route::get('/Suspended_member', 'admin\network\NetworkController@suspended_member')->name('network.suspended_member');
Route::get('/email_setting', 'admin\network\NetworkController@email_setting')->name('network.email_setting');
Route::get('/currency_setting', 'admin\network\NetworkController@currency_setting')->name('network.currency_setting')->middleware('role');
Route::get('/general_setting', 'admin\setting\SettingController@create')->name('setting.create');
Route::post('/setting_update', 'admin\setting\SettingController@update')->name('setting.update');
Route::get('/site_setting', 'admin\setting\SettingController@index')->name('setting.index');
Route::get('/site_setting_edit/{id}', 'admin\setting\SettingController@edit');
Route::get('/sub_admin', 'admin\network\NetworkController@sub_admin')->name('network.sub_admin');
Route::post('/add_sub_admin', 'admin\network\NetworkController@add_sub_admin')->name('network.add_sub_admin');
Route::get('/sub_admin_list', 'admin\network\NetworkController@sub_admin_list')->name('network.sub_admin_list');
Route::get('/edit_sub_admin/{id}', 'admin\network\NetworkController@edit_sub_admin');
Route::post('/update_sub_admin', 'admin\network\NetworkController@update_sub_admin')->name('network.update_sub_admin');
Route::get('/add_package','admin\package\PackageController@create')->name('package.create');
Route::post('/package_store','admin\package\PackageController@store')->name('package.store');
Route::get('/package_list','admin\package\PackageController@index')->name('package.index');
Route::post('/status_update_package', 'admin\package\PackageController@status_update')->name('package.status_update');
Route::get('/manage_package','admin\package\PackageController@manage_package')->name('package.manage_package');
Route::get('/edit_package/{id}', 'admin\package\PackageController@edit');
Route::post('/update_package','admin\package\PackageController@update')->name('package.update');
Route::get('/create','admin\referal\ReferalController@create')->name('referal.create');
Route::post('/store','admin\referal\ReferalController@store')->name('referal.store');
Route::get('/compensation-setting/referal-compensation/{id}','admin\referal\ReferalController@show');
Route::get('/edit/{id}', 'admin\referal\ReferalController@edit');
Route::post('/update','admin\referal\ReferalController@update')->name('referal.update');
Route::get('/create','admin\matrix\MatrixController@create')->name('matrix.create');
Route::post('/store','admin\matrix\MatrixController@store')->name('matrix.store');
Route::get('/index','admin\matrix\MatrixController@index')->name('matrix.index');
Route::get('/matrix_edit/{id}', 'admin\matrix\MatrixController@edit');
Route::post('/matrix_update','admin\matrix\MatrixController@update')->name('matrix.update');
Route::get('/create','admin\residual\ResidualController@create')->name('residual.create');
Route::post('/store','admin\residual\ResidualController@store')->name('residual.store');
Route::get('/show_residual','admin\residual\ResidualController@index')->name('residual.index');
Route::get('/edit/{id}', 'admin\residual\ResidualController@edit');
Route::post('/update','admin\residual\ResidualController@update')->name('residual.update');
Route::get('/create','admin\pool\PoolController@create')->name('pool.create');
Route::post('/store','admin\pool\PoolController@store')->name('pool.store');
Route::get('/compensation-setting/show_pool/{id}','admin\pool\PoolController@show');
Route::get('/edit/{id}', 'admin\pool\PoolController@edit');
Route::post('/update','admin\pool\PoolController@update')->name('pool.update');
Route::get('/admin-create','admin\adminbonus\AdminbonusController@create')->name('admin.create');
Route::post('/admin-store','admin\adminbonus\AdminbonusController@store')->name('admin.store');
Route::get('/show_adminbonus','admin\adminbonus\AdminbonusController@index')->name('admin.index');
Route::get('/setting-admin-bonus-edit/{id}', 'admin\adminbonus\AdminbonusController@edit');
Route::post('/setting-admin-bonus-update','admin\adminbonus\AdminbonusController@update')->name('admin.update');
Route::get('/setting/admin-penalty-create','admin\adminpenalty\AdminpenaltyController@create')->name('admin_penalty.create');
Route::post('/setting/admin-penalty-store','admin\adminpenalty\AdminpenaltyController@store')->name('admin_penalty.store');
Route::get('/show_adminpenalty','admin\adminpenalty\AdminpenaltyController@index')->name('admin_penalty.index');
Route::get('/setting/admin-penalty-edit/{id}', 'admin\adminpenalty\AdminpenaltyController@edit');
Route::post('/setting/admin-penalty-update','admin\adminpenalty\AdminpenaltyController@update')->name('admin_penalty.update');
Route::get('/compensation-setting/matrix-compensation/{id}',
	'admin\compensation\CompensationController@matrix_setting');

Route::get('/compensation-setting/residual-compensation/{id}',
	'admin\compensation\CompensationController@matrix_setting');

Route::get('/compensation-setting/residual-compensation','admin\compensation\CompensationController@residual_setting')->name('residual.setting');

Route::get('/compensation-setting/rank-setting/{id}',function()
{
	return view('admin.setting.ranksetting.add');
});

Route::get('/compensation-setting','admin\compensation\CompensationController@index')->name('compensation.show');

Route::post('/store','admin\Rank_settingController@store')->name('rank.store');


Route::get('/show_rank','admin\Rank_settingController@index')->name('rank.index');


Route::get('/edit/{id}', 'admin\Rank_settingController@edit');


Route::post('/update','admin\Rank_settingController@update')->name('rank.update');


Route::get('/compensation-settings/level-commissions/{id}','admin\compensation\CompensationController@level_commissions_setting');


Route::get('/compensation-setting/plan-configuration/{id}','admin\compensation\PlanController@show');
Route::post('/compensation-setting/plan-store','admin\compensation\PlanController@store')->name('compensation.store');


Route::get('/compensation-setting/pool-configuration/{id}','admin\compensation\PoolController@show');

Route::post('/compensation-setting/pool-setting','admin\compensation\PoolController@store')->name('pool.store');

Route::get('/compensation-setting/payout-configuration/{id}','admin\compensation\Payout_Controller@show');

Route::post('/compensation-setting/payout-setting','admin\compensation\Payout_Controller@store')->name('plan.store');


Route::get('/genealogy-tree','admin\Genealogy_Controller@index')->name('genealogy-tree.index');
Route::get('/database_backup','admin\setting\SettingController@database_backup')->name('database_backup');

Route::get('/database-backup',function()
{
	return view('admin.setting.databasebackup.backup');
});


Route::post('/verify_user','admin\network\NetworkController@verify_user')->name('plan.verify_user');
Route::get('compensation-setting/funds-transfer','admin\compensation\Fund_Transfer_SettingController@create')->name('funds-transfer-setting.create');

Route::post('compensation-setting/funds-transfer-insert','admin\compensation\Fund_Transfer_SettingController@store')->name('funds-transfer-setting.store');

Route::get('compensation-setting/show-funds-transfer-setting/{id}','admin\compensation\Fund_Transfer_SettingController@show');

Route::get('/funds-transfer/edit/{id}', 'admin\compensation\Fund_Transfer_SettingController@edit');

Route::post('compensation-setting/funds-transfer-update','admin\compensation\Fund_Transfer_SettingController@update')->name('funds-transfer-setting.update');


Route::post('/compensation_settings/level_commissions', 'admin\compensation\Compensation_packageController@store')->name('level.store');


Route::post('/dsf','admin\compensation\Compensation_packageController@my_residual')->name('pak.insert');

//Country Routes
Route::get('country-list','admin\CountryController@index')->name('country.index');
Route::get('add-country','admin\CountryController@create')->name('country.create');
Route::post('add-to','admin\CountryController@store')->name('country.store');
Route::post('/country_status','admin\CountryController@status_update')->name('country.status_update');
Route::get('inactive_country','admin\CountryController@inactive_country')->name('country.inactive_country');
Route::get('/edit_country/{id}', 'admin\CountryController@edit');
Route::post('/country_update','admin\CountryController@update')->name('country.update');
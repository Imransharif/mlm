<?php
Route::get('/direct_referal_bonus','admin\referal\ReferalController@direct_referal_bonus')->name('referal.direct_referal_bonus');
Route::get('/matrix_bonus','admin\matrix\MatrixController@matrix_bonus')->name('referal.matrix_bonus');
Route::get('/residul_bonus','admin\residual\ResidualController@residul_bonus')->name('residul.residul_bonus');

Route::get('/amount-Tranfer',function()
{
	return view('user.finanical.member_to_member_trasfer.add');
});


Route::post('/Tranfer-amount','user\Member_to_member_tansferController@store')->name('Tranfer-amount.store');
Route::get('/Tranfer_amount','user\Member_to_member_tansferController@create')->name('Tranfer-amount.create');

Route::post('/verify_trafer_amount','user\Member_to_member_tansferController@verify_trafer_amount')->name('verify_trafer.amount');

Route::get('/Transfer-History','user\Member_to_member_tansferController@index')->name('transfer-history.history');
Route::get('/trafer_amount_edit/{id}','user\Member_to_member_tansferController@edit');
Route::post('/trafer_amount_history','user\Member_to_member_tansferController@status_update')->name('trafer_amount.status');
Route::post('/trafer_amount_update','user\Member_to_member_tansferController@update')->name('trafer_amount.update');

Route::get('/Kyc-Document','user\KycController@create')->name('kyc_document.create');
Route::post('Profile/Kyc-Document','user\KycController@store')->name('kyc_document.store');
Route::get('Profile/Kyc-Document-detail','user\KycController@index')->name('kyc_document.index');
Route::get('Member-management/kyc-document-detail','user\KycController@user_kyc')->name('kyc_document.user_kyc');
Route::post('kyc-status-update','user\KycController@status_update')->name('kyc.status_update');
Route::get('/kyc-edit-document/{id}','user\KycController@edit');
Route::post('kyc-update','user\KycController@update')->name('kyc.update');
Route::get('/download_image/{image}','user\KycController@download_image');
?>
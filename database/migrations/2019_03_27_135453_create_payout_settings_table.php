<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayoutSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payout_settings', function (Blueprint $table) {
            $table->bigIncrements('payout_id');
            $table->string('payout_duration')->nullable();
            $table->string('minimum_payout_amount')->nullable();
            $table->string('maximum_payout_amount')->nullable();
            $table->string('payout_charges')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->string('created_at')->default(now());
            $table->string('updated_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payout_settings');
    }
}

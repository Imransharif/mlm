<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank__settings', function (Blueprint $table) {
            $table->bigIncrements('rank_id');
            $table->string('rank_name')->nullable();
            $table->string('rank_amount')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->string('created_at')->default(now());
            $table->string('updated_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rank__settings');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminPenaltiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_penalties', function (Blueprint $table) {
            $table->bigIncrements('admin_penalty_id');
            $table->string('admin_penalty')->nullable();
            $table->string('user_name');
            $table->tinyInteger('status')->default(0);
            $table->string('created_at')->default(now());
            $table->string('updated_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_penalties');
    }
}

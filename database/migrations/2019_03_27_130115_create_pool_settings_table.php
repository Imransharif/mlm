<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoolSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pool_settings', function (Blueprint $table) {
            $table->bigIncrements('pool_id');
            $table->integer('pool_amount')->nullable();
            $table->integer('pool_commision')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->string('created_at')->default(now());
            $table->string('updated_at')->default(now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pool_settings');
    }
}

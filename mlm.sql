-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2019 at 05:06 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mlm`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` bigint(20) UNSIGNED NOT NULL,
  `admin_fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_bonuses`
--

CREATE TABLE `admin_bonuses` (
  `admin_bonus_id` bigint(20) UNSIGNED NOT NULL,
  `admin__bonus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:03',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:03'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_penalties`
--

CREATE TABLE `admin_penalties` (
  `admin_penalty_id` bigint(20) UNSIGNED NOT NULL,
  `admin_penalty` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:03',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:03'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `compensation`
--

CREATE TABLE `compensation` (
  `compensation_id` bigint(20) UNSIGNED NOT NULL,
  `compensation_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `compensation_url` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `compensation`
--

INSERT INTO `compensation` (`compensation_id`, `compensation_name`, `compensation_url`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Matrix Level Commissions when new member(s) enrolles in downline', 'compensation-setting/matrix-compensation', 1, NULL, NULL, NULL),
(2, 'Residual Commissions', 'compensation-setting/residual-compensation', 1, NULL, NULL, NULL),
(3, 'Referral Commissions / Sponsor Commissions', 'compensation-setting/referal-compensation', 1, NULL, NULL, NULL),
(4, 'Rank Setting', 'compensation-setting/rank-setting', 1, NULL, NULL, NULL),
(5, 'Pool Commissions', 'compensation-setting/show_pool', 1, NULL, NULL, NULL),
(6, 'Fund Transfer Setting', 'compensation-setting/show-funds-transfer-setting', 1, NULL, NULL, NULL),
(7, 'Plan Configuraton', 'compensation-setting/plan-configuration', 1, NULL, NULL, NULL),
(8, 'Payout Configuraton', 'compensation-setting/payout-configuration', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `compensation_packages`
--

CREATE TABLE `compensation_packages` (
  `cp_id` bigint(20) UNSIGNED NOT NULL,
  `compensation_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `max_level_commision` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `compensation_packages`
--

INSERT INTO `compensation_packages` (`cp_id`, `compensation_id`, `package_id`, `max_level_commision`, `created_at`, `updated_at`) VALUES
(28, 1, 1, 1, '2019-04-08 02:26:45', '2019-04-08 02:26:45'),
(29, 1, 2, 2, '2019-04-08 02:27:05', '2019-04-08 02:27:05'),
(30, 2, 2, 2, '2019-04-08 02:32:09', '2019-04-08 02:32:09'),
(31, 2, 1, 3, '2019-04-08 02:33:47', '2019-04-08 02:33:47');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `country_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `country_code`, `country_image`, `country_status`, `created_at`, `updated_at`) VALUES
(2, 'Okara', '12', '1554723889.png', 1, '2019-04-08 05:57:55', '2019-04-08 06:44:49'),
(3, 'Pakistan', '34', '1554721156.png', 1, '2019-04-08 05:59:16', '2019-04-08 06:46:06'),
(4, 'Emma Ruiz', '48', '1554724590.png', 1, '2019-04-08 06:56:30', '2019-04-08 06:56:30');

-- --------------------------------------------------------

--
-- Table structure for table `fund_transfer_settings`
--

CREATE TABLE `fund_transfer_settings` (
  `transfer_amount_id` bigint(20) UNSIGNED NOT NULL,
  `minimum_transfer_amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maximum_transfer_amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:06',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:06'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fund_transfer_settings`
--

INSERT INTO `fund_transfer_settings` (`transfer_amount_id`, `minimum_transfer_amount`, `maximum_transfer_amount`, `status`, `created_at`, `updated_at`) VALUES
(1, '10', '100', 1, '2019-04-03 10:26:06', '2019-04-03 10:26:06');

-- --------------------------------------------------------

--
-- Table structure for table `kycs`
--

CREATE TABLE `kycs` (
  `kyc_id` bigint(20) UNSIGNED NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kyc_document` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kycs`
--

INSERT INTO `kycs` (`kyc_id`, `user_name`, `kyc_document`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(15, 'adminadmin12345', '1554297456.jpg', 1, 'adminadmin12345', '2019-04-03 07:05:10', '2019-04-03 08:17:55'),
(16, 'adminadmin12345', '1554458023.jpg', 0, 'adminadmin12345', '2019-04-05 04:53:43', '2019-04-05 04:53:43'),
(17, 'cananesi', '1554550210.jpg', 1, 'cananesi', '2019-04-06 06:30:10', '2019-04-06 06:31:23');

-- --------------------------------------------------------

--
-- Table structure for table `level_vise_commisions`
--

CREATE TABLE `level_vise_commisions` (
  `commision_level_id` bigint(20) UNSIGNED NOT NULL,
  `cp_id` int(11) DEFAULT NULL,
  `cp_level_percentage` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `level_vise_commisions`
--

INSERT INTO `level_vise_commisions` (`commision_level_id`, `cp_id`, `cp_level_percentage`, `created_at`, `updated_at`) VALUES
(18, 28, 1, '2019-04-08 02:26:45', '2019-04-08 02:26:45'),
(19, 29, 1, '2019-04-08 02:27:05', '2019-04-08 02:27:05'),
(20, 29, 1, '2019-04-08 02:27:05', '2019-04-08 02:27:05'),
(21, 30, 1, '2019-04-08 02:32:09', '2019-04-08 02:32:09'),
(22, 30, 1, '2019-04-08 02:32:09', '2019-04-08 02:32:09'),
(23, 31, 1, '2019-04-08 02:33:48', '2019-04-08 02:33:48');

-- --------------------------------------------------------

--
-- Table structure for table `member_to_member_transfers`
--

CREATE TABLE `member_to_member_transfers` (
  `mtmt_id` bigint(20) UNSIGNED NOT NULL,
  `mtmt_amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transferby_user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transferto_user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:06',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:06'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member_to_member_transfers`
--

INSERT INTO `member_to_member_transfers` (`mtmt_id`, `mtmt_amount`, `transferby_user_name`, `transferto_user_name`, `status`, `created_at`, `updated_at`) VALUES
(1, '15', 'adminadmin12345', 'adminadmin12345', 1, '2019-04-03 10:26:06', '2019-04-05 10:28:01'),
(2, '12', 'adminadmin12345', 'adminadmin12345', 0, '2019-04-03 10:26:06', '2019-04-06 11:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_20_093834_create_site_settings_table', 1),
(4, '2019_03_21_075319_create_packages_table', 1),
(5, '2019_03_21_105201_create_referal_bonuses_table', 1),
(6, '2019_03_21_130650_create_matrix_bonuses_table', 1),
(7, '2019_03_22_100054_create_residual_bonuses_table', 1),
(8, '2019_03_22_110628_create_pool_bonuses_table', 1),
(9, '2019_03_22_125702_create_admin_bonuses_table', 1),
(10, '2019_03_22_143659_create_admin_penalties_table', 1),
(11, '2019_03_23_135047_create_compensation_table', 1),
(12, '2019_03_25_092154_create_rank__settings_table', 1),
(13, '2019_03_26_080249_create_basic_levels_table', 1),
(14, '2019_03_26_091523_create_starter_levels_table', 1),
(15, '2019_03_26_094701_create_advance_levels_table', 1),
(16, '2019_03_26_120125_create_elite_levels_table', 1),
(17, '2019_03_27_105210_create_plan_settings_table', 1),
(18, '2019_03_27_130115_create_pool_settings_table', 1),
(19, '2019_03_27_135453_create_payout_settings_table', 1),
(20, '2019_03_28_143148_create_member_to_member_transfers_table', 1),
(21, '2019_04_01_093653_create_fund_transfer_settings_table', 1),
(22, '2019_04_02_140353_create_kycs_table', 1),
(23, '2019_04_05_143549_create_compensation_table', 2),
(24, '2019_04_06_100946_create_compensation_packages_table', 3),
(25, '2019_04_06_103637_create_level_vise_commisions_table', 4),
(26, '2019_04_08_102209_create_countries_table', 5),
(27, '2019_04_09_131045_create_admins_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `package_id` bigint(20) UNSIGNED NOT NULL,
  `package_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `package_amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `package_period` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `package_tax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:01',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:01'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`package_id`, `package_name`, `package_amount`, `package_period`, `package_tax`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Starter', '45', '60', '23', 1, '2019-04-03 10:26:01', '2019-04-03 10:26:01'),
(2, 'Basic', '150', '650', '25', 1, '2019-04-03 10:26:01', '2019-04-03 10:26:01'),
(3, 'Advance', '500', '650', '40', 1, '2019-04-03 10:26:01', '2019-04-03 10:26:01'),
(4, 'Elite', '1000', '650', '50', 1, '2019-04-03 10:26:01', '2019-04-03 10:26:01'),
(5, 'Ultra', '2000', '650', '80', 1, '2019-04-03 10:26:01', '2019-04-03 10:26:01'),
(6, 'Master', '5000', '650', '100', 1, '2019-04-03 10:26:01', '2019-04-03 10:26:01'),
(7, 'Premium', '7000', '650', '150', 1, '2019-04-03 10:26:01', '2019-04-03 10:26:01'),
(8, 'Gold', '100000', '650', '250', 1, '2019-04-03 10:26:01', '2019-04-03 10:26:01'),
(9, 'Platinum', '25000', '650', '350', 1, '2019-04-03 10:26:01', '2019-04-03 10:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payout_settings`
--

CREATE TABLE `payout_settings` (
  `payout_id` bigint(20) UNSIGNED NOT NULL,
  `payout_duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minimum_payout_amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maximum_payout_amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payout_charges` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:06',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:06'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `plan_settings`
--

CREATE TABLE `plan_settings` (
  `plan_id` bigint(20) UNSIGNED NOT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:05',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:05'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `plan_settings`
--

INSERT INTO `plan_settings` (`plan_id`, `width`, `height`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 1, '2019-04-03 10:26:05', '2019-04-03 10:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `pool_bonuses`
--

CREATE TABLE `pool_bonuses` (
  `pool_id` bigint(20) UNSIGNED NOT NULL,
  `pool_bonus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:03',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:03'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pool_settings`
--

CREATE TABLE `pool_settings` (
  `pool_id` bigint(20) UNSIGNED NOT NULL,
  `pool_amount` int(11) DEFAULT NULL,
  `pool_commision` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:05',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:05'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rank__settings`
--

CREATE TABLE `rank__settings` (
  `rank_id` bigint(20) UNSIGNED NOT NULL,
  `rank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rank_amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:04',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:04'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `referal_bonuses`
--

CREATE TABLE `referal_bonuses` (
  `referal_id` bigint(20) UNSIGNED NOT NULL,
  `referal_bonus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:02',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:02'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sit_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sit_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:01',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:01'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `sit_name`, `sit_logo`, `company_address`, `created_at`, `updated_at`) VALUES
(1, 'Ght', '1553089791.png', 'lahore', '2019-04-03 10:26:01', '2019-04-03 10:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_fname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_sname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_package` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_commision` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_sopnser_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_referal_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_referal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_wallet_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_bitcoin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `user_perfect_money` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:00',
  `updated_at` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '2019-04-03 10:26:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `user_fname`, `user_lname`, `user_sname`, `user_username`, `user_phone`, `user_package`, `user_image`, `user_commision`, `user_sopnser_id`, `user_referal_id`, `user_referal_code`, `user_address`, `user_country`, `user_wallet_address`, `user_bitcoin`, `level`, `user_perfect_money`, `status`, `role`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'webdeveloper631@gmail.com', '$2y$10$gzFIlopWtrUFD6jL7Z7KxensW1UKVYU0ytjeOb7di/sfJoHiqYY.i', 'Imran', 'Sharif', NULL, 'cananesi', NULL, NULL, 'avatar5.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', NULL, NULL, '2019-04-03 10:26:39', '2019-04-03 10:26:39'),
(2, 'admin@gamil.com', '$2y$10$JrNiQSk0orSH6Z9kUOuRLu8jOZIVI.GWsAK40u7YKsH6S9U0ubZsi', 'Ali', 'Raza', NULL, 'adminadmin12345', NULL, NULL, 'avatar5.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', NULL, NULL, '2019-04-03 10:40:18', '2019-04-03 10:40:18'),
(9, 'imransharif98@yahoo.com', '$2y$10$uKivi0frSx3cclLvUemRqexBk35BjtSzQi4UHcSlk.NP91dovDOzW', 'imransharif', 'Hope Mercer', 'pakistan', 'imransharif', '+1 (518) 358-8693', NULL, '1554803707.png', NULL, '1', NULL, '5cac6c0c5c819', NULL, 'Okara', NULL, '54634terter', NULL, NULL, 1, 'user', NULL, NULL, '2019-04-03 10:26:00', '2019-04-03 10:26:00'),
(10, 'gulu@mailinator.net', '$2y$10$jdeA7Vxn0izemt6kmSXBJ.63Z8Ok8p14MS.QlwlbN5F2b.dx23M36', 'Illiana Lawrence', 'Demetria Day', 'Audra Shelton', 'cafuwe', '+1 (808) 267-2282', NULL, '1554884993.jpg', NULL, '1', NULL, '5cada9d33bd2f', NULL, 'Emma Ruiz', NULL, '54634terter', NULL, NULL, 0, 'user', NULL, NULL, '2019-04-03 10:26:00', '2019-04-03 10:26:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_bonuses`
--
ALTER TABLE `admin_bonuses`
  ADD PRIMARY KEY (`admin_bonus_id`);

--
-- Indexes for table `admin_penalties`
--
ALTER TABLE `admin_penalties`
  ADD PRIMARY KEY (`admin_penalty_id`);

--
-- Indexes for table `compensation`
--
ALTER TABLE `compensation`
  ADD PRIMARY KEY (`compensation_id`);

--
-- Indexes for table `compensation_packages`
--
ALTER TABLE `compensation_packages`
  ADD PRIMARY KEY (`cp_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `fund_transfer_settings`
--
ALTER TABLE `fund_transfer_settings`
  ADD PRIMARY KEY (`transfer_amount_id`);

--
-- Indexes for table `kycs`
--
ALTER TABLE `kycs`
  ADD PRIMARY KEY (`kyc_id`);

--
-- Indexes for table `level_vise_commisions`
--
ALTER TABLE `level_vise_commisions`
  ADD PRIMARY KEY (`commision_level_id`);

--
-- Indexes for table `member_to_member_transfers`
--
ALTER TABLE `member_to_member_transfers`
  ADD PRIMARY KEY (`mtmt_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payout_settings`
--
ALTER TABLE `payout_settings`
  ADD PRIMARY KEY (`payout_id`);

--
-- Indexes for table `plan_settings`
--
ALTER TABLE `plan_settings`
  ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `pool_bonuses`
--
ALTER TABLE `pool_bonuses`
  ADD PRIMARY KEY (`pool_id`);

--
-- Indexes for table `pool_settings`
--
ALTER TABLE `pool_settings`
  ADD PRIMARY KEY (`pool_id`);

--
-- Indexes for table `rank__settings`
--
ALTER TABLE `rank__settings`
  ADD PRIMARY KEY (`rank_id`);

--
-- Indexes for table `referal_bonuses`
--
ALTER TABLE `referal_bonuses`
  ADD PRIMARY KEY (`referal_id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_username_unique` (`user_username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_bonuses`
--
ALTER TABLE `admin_bonuses`
  MODIFY `admin_bonus_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_penalties`
--
ALTER TABLE `admin_penalties`
  MODIFY `admin_penalty_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `compensation`
--
ALTER TABLE `compensation`
  MODIFY `compensation_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `compensation_packages`
--
ALTER TABLE `compensation_packages`
  MODIFY `cp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fund_transfer_settings`
--
ALTER TABLE `fund_transfer_settings`
  MODIFY `transfer_amount_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kycs`
--
ALTER TABLE `kycs`
  MODIFY `kyc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `level_vise_commisions`
--
ALTER TABLE `level_vise_commisions`
  MODIFY `commision_level_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `member_to_member_transfers`
--
ALTER TABLE `member_to_member_transfers`
  MODIFY `mtmt_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `package_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `payout_settings`
--
ALTER TABLE `payout_settings`
  MODIFY `payout_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plan_settings`
--
ALTER TABLE `plan_settings`
  MODIFY `plan_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pool_bonuses`
--
ALTER TABLE `pool_bonuses`
  MODIFY `pool_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pool_settings`
--
ALTER TABLE `pool_settings`
  MODIFY `pool_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rank__settings`
--
ALTER TABLE `rank__settings`
  MODIFY `rank_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `referal_bonuses`
--
ALTER TABLE `referal_bonuses`
  MODIFY `referal_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
